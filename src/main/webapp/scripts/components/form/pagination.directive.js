/* globals $ */
'use strict';

angular.module('helloJhipsterApp')
    .directive('helloJhipsterAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
