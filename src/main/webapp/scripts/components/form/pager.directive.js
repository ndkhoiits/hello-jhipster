/* globals $ */
'use strict';

angular.module('helloJhipsterApp')
    .directive('helloJhipsterAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
